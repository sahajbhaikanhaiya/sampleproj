
Pod::Spec.new do |s|

  s.name         = "SampleProj"
  s.version      = "0.0.2"
  s.summary      = "A short description of SampleProj."

  s.description  = "A comparitively long description about the project that you are uploading on the trunk."

  s.homepage     = "http://www.google.com/"

  s.license      = "MIT"
  #s.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  s.platform     = :ios, "9.0"

  s.author             = { "Sahaj" => "sahaj.bhaikanhaiya@gmail.com" }

  s.source       = { :git => "https://sahajbhaikanhaiya@bitbucket.org/sahajbhaikanhaiya/sampleproj.git", :tag => "#{s.version}" }

  s.source_files  = "SampleProj/**/*.{h,m,swift}"

  s.framework  = "UIKit"

end
